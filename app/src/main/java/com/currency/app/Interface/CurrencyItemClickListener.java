package com.currency.app.Interface;


import com.currency.app.Model.Currency;

public interface CurrencyItemClickListener {

    void onCurrencyItemClick(Currency c);
}
