package com.currency.app.Main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.currency.app.Adapter.CurrencyAdapter;
import com.currency.app.Historical.HistoricalActivity;
import com.currency.app.Interface.CurrencyItemClickListener;
import com.currency.app.Model.Currency;
import com.currency.app.Model.CurrencyExchange;
import com.currency.app.R;
import com.currency.app.Service.CurrencyExchangeService;
import com.currency.app.Utils.Common;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements Callback<CurrencyExchange>, CurrencyItemClickListener,
             Spinner.OnItemSelectedListener{

    private Spinner spinnerCurrency;
    private TextView tvLoading;
    private ProgressBar progressBar;
    private ListView lvCurrency;
    private String base = "USD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        spinnerCurrency = (Spinner) findViewById(R.id.spinnerCurrency);
        progressBar     = (ProgressBar) findViewById(R.id.progressBar);
        tvLoading       = (TextView) findViewById(R.id.tvLoading);

        lvCurrency = (ListView) findViewById(R.id.lvCurrency);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(MainActivity.this,
                R.array.currency, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerCurrency.setAdapter(arrayAdapter);
        spinnerCurrency.setOnItemSelectedListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadCurrencyExchangeData(base);
    }

    private void loadCurrencyExchangeData(String base){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CurrencyExchangeService service = retrofit.create(CurrencyExchangeService.class);
        Call<CurrencyExchange> call = service.getCurrencyExchange(base);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<CurrencyExchange> call, Response<CurrencyExchange> response) {

        //Toast.makeText(this, response.body().getBase(), Toast.LENGTH_LONG).show();

        CurrencyExchange currencyExchange = response.body();

        if (currencyExchange != null) {
            lvCurrency.setAdapter(new CurrencyAdapter(this, currencyExchange.getCurrencyList(),this));
        }

        lvCurrency.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        tvLoading.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(Call<CurrencyExchange> call, Throwable t) {
        //Toast.makeText(this, t.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCurrencyItemClick(Currency c) {

        //Toast.makeText(this, c.getName(), Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, HistoricalActivity.class);
        intent.putExtra("currencyName", c.getName());
        intent.putExtra("currencyRate", c.getRate());
        intent.putExtra("base", base);

        startActivity(intent);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        base = spinnerCurrency.getSelectedItem().toString();

        progressBar.setVisibility(View.VISIBLE);
        tvLoading.setVisibility(View.VISIBLE);
        lvCurrency.setVisibility(View.INVISIBLE);

        loadCurrencyExchangeData(base);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        spinnerCurrency.setSelection(0);
    }

}
