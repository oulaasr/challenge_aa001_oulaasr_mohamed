package com.currency.app.Service;

import com.currency.app.Model.CurrencyExchange;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface CurrencyExchangeService {

    // Get Latest Rates
    @GET("latest")
    Call<CurrencyExchange> getCurrencyExchange(@Query("base") String base);


    //start_at=2018-01-01&end_at=2018-09-01&symbols=USD,EUR&base=USD

    @GET("history")
    Call<JsonObject> getCurrencyHistorical(@Query("start_at") String start_at,
                                           @Query("end_at")   String end_at,
                                           @Query("symbols")  String symbols,
                                           @Query("base")     String base);

}
