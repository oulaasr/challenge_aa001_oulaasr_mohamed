package com.currency.app.Historical;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.currency.app.Model.Graph;
import com.currency.app.Model.Historical;
import com.currency.app.R;
import com.currency.app.Service.CurrencyExchangeService;
import com.currency.app.Utils.Common;
import com.github.mikephil.charting.charts.BarChart;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.JsonObject;

import org.json.JSONException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HistoricalActivity extends AppCompatActivity implements Callback<JsonObject> {

    private TextView tvBaseCurrency, tvExchangeCurrency;
    private String exchangeCurrency,base;
    private BarChart  barChart;
    private ArrayList tauxList;
    private ArrayList dateList;

    public List<Graph> graphList;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historical);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvBaseCurrency     = findViewById(R.id.tvBaseCurrency);
        tvExchangeCurrency = findViewById(R.id.tvExchangeCurrency);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){

            base             = bundle.getString("base");
            exchangeCurrency = bundle.getString("currencyName");

            tvBaseCurrency.setText("1" + " " + base);
            tvExchangeCurrency.setText(exchangeCurrency);
        }

        barChart = findViewById(R.id.barChart);

        tauxList  = new ArrayList();

        dateList  = new ArrayList();

        graphList = new ArrayList<>();

        loadCurrencyHistoricalData();

    }

    /**
     * Load Currency Historical Data
     */
    private void loadCurrencyHistoricalData(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CurrencyExchangeService service = retrofit.create(CurrencyExchangeService.class);
        Call<JsonObject> call = service.getCurrencyHistorical("2020-10-10",
                                                              "2020-10-20",
                                                              base + "," + exchangeCurrency,
                                                               base);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

        JsonObject jsonObject = response.body();
        Log.e("TEST Json", jsonObject.toString());

        Historical historical = new Historical();

        try {

            //Log.e("TEST Historical", historical.toString());

            if(historical.newHistorical(jsonObject.toString(), base, exchangeCurrency) != null){

                for (String key : historical.newHistorical(jsonObject.toString(), base, exchangeCurrency).getRates().keySet()) {

                    Graph graph = new Graph();

                    graph.setDate(key);
                    graph.setTaux((float)historical.newHistorical(jsonObject.toString(), base, exchangeCurrency).getRates().get(key).getCurrencyExchange());
                    graphList.add(graph);
                }

                for(int i = 0; i < graphList.size(); i++) {

                    dateList.add(graphList.get(i).getDate());
                    tauxList.add(new BarEntry(graphList.get(i).getTauxRound(), i));
                }

                BarDataSet barDataSet = new BarDataSet(tauxList, getResources().getString(R.string.historical_latest));
                barChart.animateY(2000);
                BarData barData = new BarData(dateList, barDataSet);
                barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                barChart.setData(barData);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Call<JsonObject> call, Throwable t) {
        //Toast.makeText(this, t.getMessage(), Toast.LENGTH_LONG).show();
        Log.e("TEST json", "Erreur" + t.toString());
    }


}
