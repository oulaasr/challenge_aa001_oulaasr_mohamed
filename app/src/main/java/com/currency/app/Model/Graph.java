package com.currency.app.Model;

import java.math.BigDecimal;

public class Graph {

    private String date;
    private float taux;

    public Graph() {

    }

    public Graph(String date, float taux){
        this.date = date;
        this.taux = taux;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getTaux() {
        return taux;
    }

    public void setTaux(float taux) {
        this.taux = taux;
    }

    public float getTauxRound(){
        BigDecimal bd = new BigDecimal(Float.toString(taux));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        return bd.floatValue();
    }

}
