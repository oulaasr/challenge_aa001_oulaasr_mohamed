package com.currency.app.Model;

import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Historical {

	private Map<String, Rate> rates;

	private String end_at;

	private String start_at;

	private String base;

	private String baseCur;

	private String exCur;

    public String getEndAt(){
        return end_at;
    }

	public void setEndAt(String end_at){
		this.end_at = end_at;
	}

    public Map<String, Rate> getRates(){
        return rates;
    }

	public void setRates(Map<String, Rate> rates){
		this.rates = rates;
	}

    public String getStartAt(){
        return start_at;
    }

	public void setStartAt(String start_at){
		this.start_at = start_at;
	}

    public String getBase(){
        return base;
    }

    public void setBase(String base){
		this.base = base;
	}

	@Override
 	public String toString(){
		return 
			"Historical{" + 
			"end_at = '" + end_at + '\'' +
			",rates = '" + rates + '\'' + 
			",start_at = '" + start_at + '\'' +
			",base = '" + base + '\'' + 
			"}";
	}

	public class Rate {

		private double currency_base;
		private double currency_exchange;

		public Rate(double currency_base, double currency_exchange) {
			this.currency_base     = currency_base;
			this.currency_exchange = currency_exchange;
		}

		public double getCurrencyBase() {
			return currency_base;
		}

		public void setCurrencyBase(double currency_base) {
			this.currency_base = currency_base;
		}

		public double getCurrencyExchange() {
			return currency_exchange;
		}

		public void setCurrencyExchange(double currency_exchange) {
			this.currency_exchange = currency_exchange;
		}

		@Override
		public String toString() {
			return "Rate{" +
					"currency_base=" + currency_base +
					", currency_exchange=" + currency_exchange +
					'}';
		}

		public Rate() {
		}

		public Rate newInstance(String json, String base, String selected) {

			Rate rate = new Rate();
			JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
			Double mbaseCurrency     = Double.parseDouble(String.valueOf(jsonObject.get(base)));
			Double mSelectedCurrency = Double.parseDouble(String.valueOf(jsonObject.get(selected)));
			rate.setCurrencyBase(mbaseCurrency);
			rate.setCurrencyExchange(mSelectedCurrency);
			return rate;
		}

	}

	public Historical() {

	}

	public Historical newHistorical(String json, String baseCurrency,String selectedCurrency) throws JSONException {

		baseCur = baseCurrency;
		exCur   = selectedCurrency;

		JsonElement jsonElement = new JsonParser().parse(json);

		JsonObject jsonObj = jsonElement.getAsJsonObject();

		Historical historical = new Historical();
		String base     = String.valueOf(jsonObj.get("base"));
		String start_at = String.valueOf(jsonObj.get("start_at"));
		String end_at   = String.valueOf(jsonObj.get("end_at"));

		historical.setBase(base);
		historical.setStartAt(start_at);
		historical.setEndAt(end_at);

		Map<String, Rate> map;
		JSONObject jsonObject = new JSONObject(String.valueOf(jsonObj.get("rates")));
		map = jsonToMap(jsonObject);
		historical.setRates(map);

		return historical;
	}

	private Map<String, Rate> jsonToMap(JSONObject json) throws JSONException {

		Map<String, Rate> retMap = new HashMap<String, Rate>();

		if(json != JSONObject.NULL) {
			retMap = toMap(json);
		}
		return retMap;
	}

	private Map<String, Rate> toMap(JSONObject object) throws JSONException {

		Map<String, Rate> map = new HashMap<String, Rate>();

		Iterator<String> keysItr = object.keys();

		while(keysItr.hasNext()) {
			String key = keysItr.next();
			Rate rate  = new Rate();
			Rate value = rate.newInstance(String.valueOf(object.get(key)), baseCur, exCur);
			map.put(key, value);

		}
		return map;
	}

}